alias ls='ls -G'
alias ls='ls -lG'

alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

# Encryption/Decryption Functions for easy use
encrypt() {
 	if [ $# -eq 1 ];then
		openssl enc -aes-256-cbc -salt -in $1 -out $1.enc
	else
		echo "Usaage encrypt [file]"
 	fi
}
decrypt() {
 	if [ $# -eq 2 ];then
		openssl enc -d -aes-256-cbc -in $1 -out $2
	else
		echo "Usaage decrypt [file] [file]"
 	fi
}
