" Put these lines at the very end of your vimrc file.
" Load all plugins now.
" Plugins need to be added to runtimepath before helptags can be generated.
packloadall
" Load all of the helptags now, after plugins have been loaded.
" All messages and errors will be ignored.
silent! helptags ALL

"set encoding to UTF-8
set encoding=utf-8

"split navigations
"nnoremap <C-J> <C-W><C-J>
"nnoremap <C-K> <C-W><C-K>
"nnoremap <C-L> <C-W><C-L>
"nnoremap <C-H> <C-W><C-H>

" Esc key is hard... jj is easier  
inoremap jj <esc>

"normal mode line inserts
nnoremap <silent> ]<Space> :<C-u>put =repeat(nr2char(10),v:count)<Bar>execute "'[-1"<CR>
nnoremap <silent> [<Space> :<C-u>put!=repeat(nr2char(10),v:count)<Bar>execute "']+1"<CR>

syntax on

set laststatus=2
filetype on
"status line options - expanded for readbility
set statusline=
set statusline+=%2*[%n%H%M%R%W]%*\
set statusline+=%F\
set statusline+=%{FugitiveStatusline()}
set statusline+=%=%1*%y%*%*\
set statusline+=%10((%l,%c)%)\
set statusline+=%P

"Python spacing autocommands
augroup python_space
    autocmd BufRead,BufNewfile *.py set expandtab
    autocmd BufRead,BufNewFile *.py set tabstop=4   
    autocmd BufRead,BufNewFile *.py set shiftwidth=4
    autocmd BufRead,BufNewFile *.py set autoindent
    autocmd BufRead,BufNewFile *.py set textwidth=79
    autocmd BufRead,BufNewFile *.py set softtabstop=4
    autocmd BufRead,BufNewFile *.py set fileformat=unix
augroup END

" Terraform Spacing
augroup tf_space
    autocmd BufRead,BufNewfile *.tf set expandtab
    autocmd BufRead,BufNewFile *.tf set tabstop=2
    autocmd BufRead,BufNewFile *.tf set shiftwidth=2
    autocmd BufRead,BufNewFile *.tf set autoindent
    autocmd BufRead,BufNewFile *.tf set softtabstop=2
    autocmd BufRead,BufNewFile *.tf set fileformat=unix
augroup END


"Yaml Spacing autocommands
augroup yaml_space
    autocmd BufRead,BufNewfile *.yml set expandtab
    autocmd BufRead,BufNewFile *.yml set tabstop=2
    autocmd BufRead,BufNewFile *.yml set shiftwidth=2
    autocmd BufRead,BufNewFile *.yml set autoindent
augroup END

"Ledger Spacing Commands
augroup ledger_space
    autocmd BufRead,BufNewfile *.ledger set expandtab
    autocmd BufRead,BufNewFile *.ledger set tabstop=2
    autocmd BufRead,BufNewFile *.ledger set shiftwidth=2
    autocmd BufRead,BufNewFile *.ledger set autoindent
augroup END

set number relativenumber
set modelines=0
set nomodeline
set foldmethod=indent
set foldlevel=99
nnoremap <space> za

let g:terraform_fmt_on_save=1
let g:terraform_align=1
let g:terraform_fold_sections=1
